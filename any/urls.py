from django.urls import path
from any.views import FirstEndpoint, SmartEndpoint, SmartEndpointDetail

urlpatterns = [
    path('', FirstEndpoint.as_view()),
    path('generics', SmartEndpoint.as_view()),
    path('generics/<int:pk>', SmartEndpointDetail.as_view())
]
