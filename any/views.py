from django.shortcuts import render

from rest_framework import generics
from rest_framework import views
from rest_framework.response import Response
from rest_framework import serializers
from rest_framework import pagination
from rest_framework import permissions
from .models import Some
from django_filters import rest_framework as filters


class SomeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Some
        fields = ('id', 'name')


class Pagy(pagination.PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'


class Fil(filters.FilterSet):
    name = filters.CharFilter('name', 'icontains')

    class Meta:
        model = Some
        fields = ('name',)


class SmartEndpoint(generics.ListCreateAPIView):
    queryset = Some.objects.all().order_by('-id')
    serializer_class = SomeSerializer
    pagination_class = Pagy
    filter_class = Fil


class SmartEndpointDetail(generics.RetrieveUpdateAPIView):
    queryset = Some.objects.all()
    serializer_class = SomeSerializer


class FirstEndpoint(views.APIView):

    def get(self, request, *args, **kwargs):

        # items = Some.objects.all()
        # print(items, type(items))
        #
        # names = [item.name for item in items]
        # print(names, type(names))

        names2 = Some.objects.values_list('name', flat=True)

        return Response(names2)
